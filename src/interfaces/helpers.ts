export interface CreateElemOptions {
  tagName: string;
  className?: string;
  attributes?: {
    [index: string]: string;
  }
}

export type FighterPosition = 'left' | 'right';

export enum FightControls {
  PlayerOneAttack = 'KeyA',
  PlayerOneBlock = 'KeyD',
  PlayerTwoAttack = 'KeyJ',
  PlayerTwoBlock = 'KeyL'
}
export type CriticalHitCombination1 = ['KeyQ', 'KeyW', 'KeyE'];
export type CriticalHitCombination2 = ['KeyU', 'KeyI', 'KeyO'];
export type KeyEvents = 'keydown' | 'keyup';

export type FighterClickFn = (event: Event, fighterId: string) => Promise<void>;
export type CloseModalFn = () => void;
export interface ModalOptions {
  title: string;
  bodyElement: HTMLElement;
  onClose: CloseModalFn;
}
