import { CreateElemOptions } from '../../interfaces/helpers';

export function createElement(params: CreateElemOptions): HTMLElement {
  const { tagName, className, attributes = {} } = params;
  const element: HTMLElement = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) => element.setAttribute(key, attributes[key]));

  return element;
}
