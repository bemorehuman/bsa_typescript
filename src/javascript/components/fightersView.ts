import { createElement } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { Fighter } from '../../interfaces/fighter';
import { FighterClickFn } from '../../interfaces/helpers';

export function createFighters(fighters: Fighter[]): HTMLElement {
  const selectFighter: FighterClickFn = createFightersSelector();
  const container = createElement({ tagName: 'div', className: 'fighters___root' });
  const preview = createElement({ tagName: 'div', className: 'preview-container___root' });
  const fightersList = createElement({ tagName: 'div', className: 'fighters___list' });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(fighter: Fighter, selectFighter: FighterClickFn): HTMLElement {
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createImage(fighter);
  const onClick = (event: Event) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: Fighter): HTMLElement {
  const { source, name } = fighter;
  const attributes: { [index: string]: string; } = {
    src: source,
    title: name,
    alt: name
  };
  return createElement({
    tagName: 'img',
    className: 'fighter___fighter-image',
    attributes
  });
}
