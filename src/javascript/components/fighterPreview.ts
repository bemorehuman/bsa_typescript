import { createElement } from '../helpers/domHelper';
import { FighterDetail } from '../../interfaces/fighter';
import { FighterPosition } from '../../interfaces/helpers';

export function createFighterPreview(fighter: FighterDetail, position: FighterPosition): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (fighter) {
    fighterElement.appendChild(createFighterImage(fighter));
    fighterElement.appendChild(createFighterInfo(fighter));
  }

  return fighterElement;
}

export function createFighterImage(fighter: FighterDetail): HTMLElement {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}

function createFighterInfo(fighter: FighterDetail): HTMLElement {
  const { name, attack, defense } = fighter;
  const fighterInfo = createElement({
    tagName: 'div',
    className: `fighter-preview___info`
  });
  fighterInfo.innerHTML = `
        <h3>${name}</h3>
        <div class="attack">Attack: <span>${attack}</span></div>
        <div class="defense">Defense: <span>${defense}</span></div>
  `;

  return fighterInfo;
}
