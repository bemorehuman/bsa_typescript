import { createElement } from '../../helpers/domHelper';
import { CloseModalFn, ModalOptions } from '../../../interfaces/helpers';

export function showModal(options: ModalOptions): void {
  const root = getModalContainer();
  const modal = createModal(options);

  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root') as HTMLElement;
}

function createModal(options: ModalOptions): HTMLElement {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(options.title, options.onClose);

  modalContainer.append(header, options.bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, onClose: CloseModalFn): HTMLElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';

  const close: CloseModalFn = () => {
    hideModal();
    onClose();
  };
  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);

  return headerElement;
}

function hideModal(): void {
  const modal: HTMLElement = document.getElementsByClassName('modal-layer')[0] as HTMLElement;
  modal?.remove();
}
