import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../../interfaces/fighter';

class FighterService {
  async getFighters(): Promise<Fighter[]> {
    try {
      const endpoint: string = 'fighters.json';
      return await callApi<Fighter[]>(endpoint, 'GET');
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<Fighter> {
    try {
      const endpoint: string = `details/fighter/${id}.json`;
      return await callApi(endpoint, 'GET');
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
